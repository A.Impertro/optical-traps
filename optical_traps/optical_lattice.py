import numpy as np
from numpy import sin, cos, exp, sqrt, pi
from .consts import c, k_B, h_bar, u, eps0
from scipy.signal import find_peaks


class LaserBeam(object):

    def __init__(self, wavelength: float, power: float, waist_x: float, waist_y: float = None, angle: float = 0,
                 xz_waistpos: tuple = (0, 0)):
        """Initializes a new LaserBeam object and calculates further beam parameters.

        Parameters
        ----------
        wavelength : float
            The laser wavelength.
        power : float
            The laser power.
        waist_x : float
            The waist size (radius) in x-direction.
        waist_y : float
            The waist size (radius) in y-direction. Defaults to 'None', resulting in a circular beam profile.
        angle : float
            The angle of the wave vector w.r.t. z-axis. Defaults to 0.
        xz_waistpos :
            The position of the beam waist in the xz-plane.
        """
        self.wavelength = wavelength
        self.power = power
        self.waist_x = waist_x
        if waist_y is None:
            self.waist_y = waist_x
        else:
            self.waist_y = waist_y
        self.angle = angle
        self.xz_waistpos = xz_waistpos

        self.angular_freq = self.i0 = self.wave_vector = self.z_R_x = self.z_R_y = self.z_R_tot = self.wx = self.wy = \
            self.roc_x = self.roc_y = self.psi = 0

        self.recalculate()

    def recalculate(self):
        self.angular_freq = 2 * np.pi * c / self.wavelength
        self.i0 = 2*self.power/np.pi/self.waist_x/self.waist_y
        self.wave_vector = 2*np.pi/self.wavelength
        self.z_R_x = np.pi*self.waist_x**2/self.wavelength
        self.z_R_y = np.pi*self.waist_y**2/self.wavelength
        self.z_R_tot = 2**(1/2) * self.z_R_x * self.z_R_y / (self.z_R_x**2 + self.z_R_y**2)**(1/2)

        self.wx = lambda z: self.waist_x * sqrt(1 + (z / self.z_R_x) ** 2) 	        # waist in x direction
        self.wy = lambda z: self.waist_y * sqrt(1 + (z / self.z_R_y) ** 2)           # waist in y direction
        self.roc_x = lambda z: z * (1 + (self.z_R_x / z) ** 2)                          # r.o.c. in x direction
        self.roc_y = lambda z: z * (1 + (self.z_R_y / z) ** 2)                          # r.o.c. in y direction
        self.psi = lambda z: 0.5 * (np.arctan(z/self.z_R_x) + np.arctan(z/self.z_R_y))  # overall Gouy phase

    def wx_rotated(self, x, z):
        wx, wz = self.xz_waistpos
        xrot = (x-wx)*cos(self.angle) + (z-wz) * sin(self.angle)
        zrot = - (x-wx)*sin(self.angle) + (z-wz) * cos(self.angle)
        return xrot - self.wx(zrot)

    def print_beam_params(self):
        """Prints the main parameters of the laser beam.

        """
        print('Wavelength: {:.2f} nm'.format(self.wavelength*1e9))
        print('Power: {:.2f} W'.format(self.power))
        print('Focal waist x: {:.2f} μm'.format(self.waist_x*1e6))
        print('Focal waist y: {:.2f} μm'.format(self.waist_y*1e6))
        if self.angle != 0:
            print('Angle: {:.2f}°'.format(self.angle*180/pi))
        print('Rayleigh range x: {:.2f} mm'.format(self.z_R_x*1e3))
        print('Rayleigh range y: {:.2f} mm'.format(self.z_R_y*1e3))
        print('')

    def e_field_value(self, x: float, y: float, z: float) -> float:
        """Calculates the electric field value at the specified (x,y,z)-coordinate.

        Parameters
        ----------
        x : float
            x-coordinate
        y : float
            y-coordinate
        z : float
            z_coordinate

        Returns
        -------
        float
            The normalized electric field value.
        """
        wx, wz = self.xz_waistpos
        xrot = (x-wx)*cos(self.angle) + (z-wz) * sin(self.angle)
        zrot = - (x-wx)*sin(self.angle) + (z-wz) * cos(self.angle)

        return (2*self.power/pi/self.wx(zrot)/self.wy(zrot))**(1/2) \
            * exp(- xrot**2/self.wx(zrot)**2 - y**2/self.wy(zrot)**2) \
            * exp(-1j*(self.wave_vector*zrot
                       + self.wave_vector*(xrot**2/2/self.roc_x(zrot)+y**2/2/self.roc_y(zrot))
                       - self.psi(zrot)))

    def intensity_dist(self, x: float, y: float, z: float) -> float:
        """Calculates the optical intensity at the specified (x,y,z)-coordinate.

        Parameters
        ----------
        x : float
            x-coordinate
        y : float
            y-coordinate
        z : float
            z_coordinate

        Returns
        -------
        float
            The optical intensity.
        """
        return np.abs(self.e_field_value(x, y, z))**2


class AccordionLattice(object):

    def __init__(self, beam_1: LaserBeam, beam_2: LaserBeam):
        """Initializes an accordion lattice trap composed of two Gaussian laser beams.

        Note that the wavelengths of both beams must be equal. Additionally, the beam angles have to be equal-valued
        and of opposite sign.

        Parameters
        ----------
        beam_1 : LaserBeam
            The first laser beam.
        beam_2 : LaserBeam
            The second laser beam.
        """
        self.beam_1 = beam_1
        self.beam_2 = beam_2

        if beam_1.angle != -beam_2.angle:
            raise NotImplementedError('Beam angles must be equal-valued and of opposite sign!')

        if beam_1.wavelength != beam_2.wavelength:
            raise NotImplementedError('Beam wavelengths must be equal!')

    def intensity_dist(self, x: float, y: float, z: float) -> float:
        """Calculates the optical intensity at the specified (x,y,z)-coordinate.

        Parameters
        ----------
        x : float
            x-coordinate
        y : float
            y-coordinate
        z : float
            z_coordinate

        Returns
        -------
        float
            The optical intensity.
        """
        return np.abs(self.beam_1.e_field_value(x, y, z) + self.beam_2.e_field_value(x, y, z))**2

    def potential_dist(self, x: float, y: float, z: float, polarizability: float):
        """Calculates the optical dipole potential with respect to the given polarizability at the specified
        (x,y,z)-coordinate.

        Parameters
        ----------
        x : float
            x-coordinate [m]
        y : float
            y-coordinate [m]
        z : float
            z_coordinate [m]
        polarizability : float
            The polarizability of a given element at the wavelength of the trap beam.

        Returns
        -------
        float
            The dipole potential [J]
        """

        return -polarizability/2/eps0/c*self.intensity_dist(x, y, z)

    def get_site_centers(self, bounds: tuple = (-250e-6, 250e-6), threshold: float = 5e-3) -> list:
        """Calculates and returns the center positions of individual lattice sites along the x-axis within the
        specified bounds.

        Parameters
        ----------
        bounds: Tuple[float, float]
            The lower and upper bounds of the region in which site centers are searched.
        threshold : float
            The fraction of the maximum height at which a site is still counted.

        Returns
        -------
        numpy.array
            A numpy array containing the x-positions of the found lattice sites.
        """
        b_min, b_max = bounds
        x = np.linspace(b_min, b_max, 10000)
        lattice_csect_x = self.intensity_dist(x, 1e-99, 1e-99)
        peaks, _ = find_peaks(lattice_csect_x, height=threshold*np.max(lattice_csect_x))
        site_centers = x[peaks]
        return site_centers

    def calc_trap_params(self, polarizability: float, m_atoms: float = 173 * u) -> tuple:
        """Calculate and return the trap frequencies as well as the trap depth.

        Parameters
        ----------
        polarizability : float
            The polarizability of a given element at the wavelength of the trap beam.
        m_atoms : float
            The mass of the atoms for which the trap frequencies are calculated. Defaults to 173*u.
        Returns
        -------
        Tuple[Tuple[float, float float], float]
            Returns a tuple of the three trap frequencies in (x,y,z)-direction and the trap depth in the center.
        """
        vt = polarizability / 2 / eps0 / c

        pa = self.beam_1.power
        pb = self.beam_2.power
        i0a = self.beam_1.i0
        i0b = self.beam_2.i0
        theta = self.beam_1.angle
        w0xa = self.beam_1.waist_x
        w0ya = self.beam_1.waist_y
        w0xb = self.beam_2.waist_x
        w0yb = self.beam_2.waist_y
        k = self.beam_1.wave_vector

        z_Rxa = self.beam_1.z_R_x
        z_Rya = self.beam_1.z_R_y
        z_Rxb = self.beam_2.z_R_x
        z_Ryb = self.beam_2.z_R_y

        v_0 = - vt * ((i0a)**(1/2) + (i0b)**(1/2))**2

        omega_x = (2*np.abs(vt)/m_atoms*(2*i0a*cos(theta)**2/w0xa**2 + 2*i0b*cos(theta)**2/w0xb**2 +
                                         i0a*sin(theta)**2*(1/2/z_Rxa**2 + 1/2/z_Rya**2) +
                                         i0b*sin(theta)**2*(1/2/z_Rxb**2 + 1/2/z_Ryb**2) +
                                         2*(i0a*i0b)**(1/2)*(1/4*sin(theta)**2*(1/z_Rxa**2+1/z_Rya**2 +
                                                                                1/z_Rxb**2+1/z_Ryb**2) +
                                                             cos(theta)**2*(1/w0xa**2+1/w0xb**2) +
                                                             1/8*sin(theta)**2*(4*k-1/z_Rxa-1/z_Rya -
                                                                                1/z_Rxb-1/z_Ryb)**2)))**(1/2)
        omega_y = (4*np.abs(vt)/m_atoms*(i0a/w0ya**2+i0b/w0yb**2 +
                                         (i0a*i0b)**(1/2)*(w0ya**2+w0yb**2)/w0ya**2/w0yb**2))**(1/2)
        omega_z = (2*np.abs(vt)/m_atoms*(2*i0a*sin(theta)**2/w0xa**2 + 2*i0b*sin(theta)**2/w0xb**2 +
                                         i0a*cos(theta)**2*(1/2/z_Rxa**2 + 1/2/z_Rya**2) +
                                         i0b*cos(theta)**2*(1/2/z_Rxb**2 + 1/2/z_Ryb**2) +
                                         2*(i0a*i0b)**(1/2)*(1/4*cos(theta)**2*(1/z_Rxa**2+1/z_Rya**2 +
                                                                                1/z_Rxb**2+1/z_Ryb**2) +
                                                             sin(theta)**2*(1/w0xa**2+1/w0xb**2) +
                                                             1/8*cos(theta)**2*(1/z_Rxa+1/z_Rya -
                                                                                1/z_Rxb-1/z_Ryb)**2)))**(1/2)

        omega_bar_3d = (omega_x * omega_y * omega_z) ** (1 / 3)

        return (omega_x, omega_y, omega_z), omega_bar_3d, v_0

    def get_envelope_frequencies(self, polarizability: float, m_atoms: float = 173 * u) -> tuple:
        """Calculate and return the trap frequencies corresponding to the envelope of the lattice

        Parameters
        ----------
        polarizability : float
            The polarizability of a given element at the wavelength of the trap beam.
        m_atoms : float
            The mass of the atoms for which the trap frequencies are calculated. Defaults to 173*u.
        Returns
        -------
        Tuple[float, float float]
            Returns a tuple of the three envelope trap frequencies in (x,y,z)-direction.

        """
        beam1 = LaserBeam(wavelength=self.beam_1.wavelength, power=2*self.beam_1.power, waist_x=self.beam_1.waist_x,
                          waist_y=self.beam_1.waist_y, angle=self.beam_1.angle, xz_waistpos=self.beam_1.xz_waistpos)
        beam2 = LaserBeam(wavelength=self.beam_2.wavelength, power=2*self.beam_2.power, waist_x=self.beam_2.waist_x,
                          waist_y=self.beam_2.waist_y, angle=self.beam_2.angle, xz_waistpos=self.beam_2.xz_waistpos)
        equivalent_odt = TwoBeamDipoleTrap(beam1, beam2)
        trap_freqs, _, _ = equivalent_odt.calc_trap_params(polarizability, polarizability, m_atoms)
        return trap_freqs


class SingleBeamDipoleTrap(object):

    def __init__(self, beam: LaserBeam):
        """Initializes a dipole trap composed of a single Gaussian laser beam.

        Parameters
        ----------
        beam : LaserBeam
            The laser beam.
        """
        self.beam = beam

    def intensity_dist(self, x: float, y: float, z: float) -> float:
        """Calculates the optical intensity at the specified (x,y,z)-coordinate.

        Parameters
        ----------
        x : float
            x-coordinate
        y : float
            y-coordinate
        z : float
            z_coordinate

        Returns
        -------
        float
            The optical intensity.
        """
        return self.beam.intensity_dist(x, y, z)

    def potential_dist(self, x: float, y: float, z: float, polarizability: float):
        """Calculates the optical dipole potential with respect to the given polarizability at the specified
        (x,y,z)-coordinate.

        Parameters
        ----------
        x : float
            x-coordinate [m]
        y : float
            y-coordinate [m]
        z : float
            z_coordinate [m]
        polarizability : float
            The polarizability of a given element at the wavelength of the trap beam.

        Returns
        -------
        float
            The dipole potential [J]
        """

        return -polarizability/2/eps0/c*self.intensity_dist(x, y, z)

    def calc_trap_params(self, polarizability: float, m_atoms: float = 173*u) -> tuple:
        """Calculate and return the trap frequencies as well as the trap depth.

        Parameters
        ----------
        polarizability : float
            The polarizability of a given element at the wavelength of the trap beam.
        m_atoms : float
            The mass of the atoms for which the trap frequencies are calculated. Defaults to 173*u.
        Returns
        -------
        Tuple[Tuple[float, float float], float]
            Returns a tuple of the three trap frequencies in (x,y,z)-direction and the trap depth in the center.
        """
        vt = polarizability / 2 / eps0 / c

        power = self.beam.power
        w0x = self.beam.waist_x
        w0y = self.beam.waist_y
        z_Rx = self.beam.z_R_x
        z_Ry = self.beam.z_R_y
        theta = self.beam.angle

        v_0 = - 2*vt*power/pi/w0x/w0y

        omega_x = (2*np.abs(vt)*power*(4*z_Rx**2*z_Ry**2*cos(theta)**2 + w0x**2*sin(theta)**2*(z_Rx**2+z_Ry**2))
                   / m_atoms/pi/w0x**3/w0y/z_Rx**2/z_Ry**2)**(1/2)
        omega_y = (8*np.abs(vt)*power/pi/m_atoms/w0x/w0y**3)**(1/2)
        omega_z = (2*np.abs(vt)*power*(4*z_Rx**2*z_Ry**2*sin(theta)**2 + w0x**2*cos(theta)**2*(z_Rx**2+z_Ry**2))
                   / m_atoms/pi/w0x**3/w0y/z_Rx**2/z_Ry**2)**(1/2)
        omega_bar_3d = (omega_x*omega_y*omega_z)**(1/3)

        return (omega_x, omega_y, omega_z), omega_bar_3d, v_0


class TwoBeamDipoleTrap(object):

    def __init__(self, beam_1: LaserBeam, beam_2: LaserBeam):
        """Initializes a dipole trap composed of two Gaussian laser beams.

        Parameters
        ----------
        beam_1 : LaserBeam
            The first laser beam.
        beam_2 : LaserBeam
            The second laser beam.
        """
        self.beam_1 = beam_1
        self.beam_2 = beam_2

    def intensity_dist(self, x: float, y: float, z: float) -> float:
        """Calculates the optical intensity at the specified (x,y,z)-coordinate.

        Parameters
        ----------
        x : float
            x-coordinate
        y : float
            y-coordinate
        z : float
            z_coordinate

        Returns
        -------
        float
            The optical intensity.
        """
        return self.beam_1.intensity_dist(x, y, z) + self.beam_2.intensity_dist(x, y, z)

    def potential_dist(self, x: float, y: float, z: float, pol_a: float, pol_b: float):
        """Calculates the optical dipole potential with respect to the given polarizability at the specified
        (x,y,z)-coordinate.

        Parameters
        ----------
        x : float
            x-coordinate [m]
        y : float
            y-coordinate [m]
        z : float
            z_coordinate [m]
        pol_a : float
            The polarizability of a given element at the wavelength of trap beam a.
        pol_b : float
            The polarizability of a given element at the wavelength of trap beam b.

        Returns
        -------
        float
            The dipole potential [J]
        """
        vt_a = pol_a / 2 / eps0 / c
        vt_b = pol_b / 2 / eps0 / c

        return -vt_a*self.beam_1.intensity_dist(x, y, z) - vt_b*self.beam_2.intensity_dist(x, y, z)

    def calc_trap_params(self, pol_a: float, pol_b: float, m_atoms: float = 173*u) -> tuple:
        """Calculate and return the trap frequencies as well as the trap depth.

        Parameters
        ----------
        pol_a : float
            The polarizability of a given element at the wavelength of trap beam a.
        pol_b : float
            The polarizability of a given element at the wavelength of trap beam b.
        m_atoms : float
            The mass of the atoms for which the trap frequencies are calculated. Defaults to 173*u.
        Returns
        -------
        Tuple[Tuple[float, float float], float]
            Returns a tuple of the three trap frequencies in (x,y,z)-direction and the trap depth in the center.
        """
        vt_a = pol_a/2/eps0/c
        vt_b = pol_b/2/eps0/c

        i_0a = self.beam_1.i0
        i_0b = self.beam_2.i0
        th_a = self.beam_1.angle
        th_b = self.beam_2.angle
        w0xa = self.beam_1.waist_x
        w0ya = self.beam_1.waist_y
        w0xb = self.beam_2.waist_x
        w0yb = self.beam_2.waist_y
        z_Rxa = self.beam_1.z_R_x
        z_Rya = self.beam_1.z_R_y
        z_Rxb = self.beam_2.z_R_x
        z_Ryb = self.beam_2.z_R_y

        v_0 = - (vt_a*i_0a + vt_b*i_0b)

        omega_x = (2*np.abs(vt_a)/m_atoms*(2*i_0a*cos(th_a)**2/w0xa**2 +
                                           i_0a*sin(th_a)**2*(1/2/z_Rxa**2 + 1/2/z_Rya**2)) +
                   2*np.abs(vt_b)/m_atoms*(2*i_0b*cos(th_b)**2/w0xb**2 +
                                           i_0b*sin(th_b)**2*(1/2/z_Rxb**2 + 1/2/z_Ryb**2)))**(1/2)
        omega_y = (4/m_atoms*(np.abs(vt_a)*i_0a/w0ya**2 + np.abs(vt_b)*i_0b/w0yb**2))**(1/2)
        omega_z = (2*np.abs(vt_a)/m_atoms*(2*i_0a*sin(th_a)**2/w0xa**2 +
                                           i_0a*cos(th_a)**2*(1/2/z_Rxa**2 + 1/2/z_Rya**2)) +
                   2*np.abs(vt_b)/m_atoms*(2*i_0b*sin(th_b)**2/w0xb**2 +
                                           i_0b*cos(th_b)**2*(1/2/z_Rxb**2 + 1/2/z_Ryb**2)))**(1/2)
        omega_bar_3d = (omega_x*omega_y*omega_z)**(1/3)

        return (omega_x, omega_y, omega_z), omega_bar_3d, v_0
