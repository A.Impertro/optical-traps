import numpy as np
from .consts import c, k_B, h_bar, u, eps0


class Transition(object):

    def __init__(self, wavelength: float, decay_rate: float):
        self.wavelength = wavelength
        self.decay_rate = decay_rate
        self.angular_frequency = 2*np.pi*c/wavelength
        self.i_sat = np.pi**2*2*h_bar*c*decay_rate/3/wavelength**3

    def get_polarizability(self, wavelength: float):
        laser_freq = 2*np.pi*c/wavelength
        detuning = self.angular_frequency - laser_freq
        sum_freq = self.angular_frequency + laser_freq

        return 3*np.pi*eps0*c**3*self.decay_rate/self.angular_frequency**3*(1/detuning + 1/sum_freq)

    def calc_scattering_rate(self, wavelength: float, intensity: float):
        s_0 = intensity / self.i_sat
        delta = 2*np.pi*c/wavelength - self.angular_frequency
        return self.decay_rate*s_0/2/(1+s_0+(2*delta/self.decay_rate)**2)
