import numpy as np
from numpy import exp
import warnings
from scipy.optimize import brentq
from .utilities import rpolylog
from .consts import c, k_B, h_bar, u


class NonIntFermiGas(object):

    def __init__(self, trap_freqs: tuple, N_atoms: int, T_atoms: float = None, T_red: float = None,
                 M_atoms: float = 173*u, regime: str = '3D', verbose=False):
        """Initialize a Fermi gas with a given N,T,M captured in a harmonic trap of specified trap frequencie(s). The
        temperature can be passed either in absolute units (T_atoms) or in units of the Fermi temperature (T_red).

        Attention: For 2D gases, the y and z directions are seen as the system extent, while for a 1D gas, the system 
        is aligned with the z direction.

        The following parameters of the Fermi gas will be computed and made available as instance variables:

        - E_F : Fermi energy

        - T_F : Fermi temperature

        - fugacity : Fugacity

        - mu : chemical potential

        - E_tot : Total energy of gas in harmonic trap

        - S_tot : Total entropy of gas in harmonic trap

        Parameters
        ----------
        trap_freqs : Tuple(float, float, float)
            Tuple containing the trap frequencies (as angular frequencies) in (x,y,z)-direction.
        N_atoms : float
            Number of atoms in the Fermi gas
        T_atoms : float
            Absolute temperature of the Fermi gas. Alternatively specify reduced temperature.
        T_red : float
            Reduced temperature (T/T_F) of the Fermi gas. Alternative specify absolute temperature.
        M_atoms : float
            Mass of the atoms used.
        regime : str
            The regime [1D, 2D, 3D] of the Fermi gas. Defaults to 3D.
        verbose : bool
            If true, prints trap frequencies and computed quantities of the Fermi gas.
        """
        if T_atoms is None and T_red is None:
            raise ValueError('Must provide either temperature in K or reduced temperature.')
        if T_atoms is not None and T_red is not None:
            raise ValueError('Providing both absolute temperature and reduced temperature is not allowed.')

        self.N_atoms = N_atoms
        self.M_atoms = M_atoms
        self.regime = regime
        if T_atoms is not None:
            self.T_atoms = T_atoms

        if len(trap_freqs) != 3:
            warnings.warn('Did not specify three trap frequencies, returning.')
            return

        self.omega_x, self.omega_y, self.omega_z = trap_freqs
        if self.regime == '3D':
            self.omega_bar = (self.omega_x*self.omega_y*self.omega_z)**(1/3)
        elif self.regime == '2D':
            self.omega_bar = (self.omega_y*self.omega_z)**(1/2)
        elif self.regime == '1D':
            self.omega_bar = self.omega_z
        else:
            raise ValueError('Unknown regime. Possibilities: [1D, 2D, 3D]')

        self.a_HO = (h_bar/M_atoms/self.omega_bar)**(1/2)
        self.R_F = self.a_HO * (48*N_atoms)**(1/6)

        if verbose:
            print('ODT Trap Frequencies:')
            print('\t ωx   = 2π * {:.2f} Hz'.format(self.omega_x / 2 / np.pi))
            print('\t ωy   = 2π * {:.2f} Hz'.format(self.omega_y / 2 / np.pi))
            print('\t ωz   = 2π * {:.2f} Hz'.format(self.omega_z / 2 / np.pi))
            print('\t ωbar = 2π * {:.2f} Hz'.format(self.omega_bar / 2 / np.pi))

        if self.regime == '3D':
            self.E_F = h_bar * self.omega_bar * (6*N_atoms)**(1/3)
            self.T_F = self.E_F/k_B
            if T_red is not None:
                self.T_atoms = T_red*self.T_F

            self.fugacity = brentq(lambda x: N_atoms+k_B**3*self.T_atoms**3*rpolylog(3, -x)
                                   / (h_bar**3 * self.omega_bar**3), 1e-10, 1e50)
            self.mu = k_B * self.T_atoms * np.log(self.fugacity)
            self.E_tot = - 3 * k_B**4 * self.T_atoms**4 * rpolylog(4, -self.fugacity)/h_bar**3/self.omega_bar**3
            self.S_tot = (self.E_tot - self.mu*N_atoms)/k_B/self.T_atoms - k_B**3 * self.T_atoms**3 / h_bar**3 / \
                self.omega_bar**3 * rpolylog(4, -self.fugacity)

        elif self.regime == '2D':
            self.E_F = h_bar * self.omega_bar * (2 * N_atoms) ** (1 / 2)
            self.T_F = self.E_F/k_B
            if T_red is not None:
                self.T_atoms = T_red*self.T_F
            self.fugacity = brentq(lambda x: N_atoms + k_B**2*self.T_atoms**2*rpolylog(2, -x)
                                   / (h_bar**2 * self.omega_bar**2), 1e-10, 1e50)
            self.mu = k_B * self.T_atoms * np.log(self.fugacity)
            self.E_tot = - 2 * k_B**3 * self.T_atoms**3 * rpolylog(3, -self.fugacity)/h_bar**2/self.omega_bar**2
            self.S_tot = (self.E_tot - self.mu*N_atoms)/k_B/self.T_atoms - k_B**2 * self.T_atoms**2 / h_bar ** 2 / \
                self.omega_bar**2 * rpolylog(3, -self.fugacity)
        elif self.regime == '1D':
            self.E_F = h_bar * self.omega_z * N_atoms
            self.T_F = self.E_F / k_B
            if T_red is not None:
                self.T_atoms = T_red*self.T_F
            self.fugacity = exp(self.T_F / self.T_atoms) - 1
            self.mu = k_B * self.T_atoms * np.log(self.fugacity)
            self.E_tot = - k_B**2 * self.T_atoms**2 * rpolylog(2, -self.fugacity) / h_bar / self.omega_z
            self.S_tot = (2 * self.E_tot - self.mu * N_atoms) / k_B / self.T_atoms
        else:
            raise ValueError('Unknown regime. Possibilities: [1D, 2D, 3D]')

        if verbose:
            print('Selected regime: ' + self.regime)
            print('Atom number: N = {:.0f}'.format(self.N_atoms))
            print('Temperature: T = {:.2f} nK'.format(self.T_atoms*1e9))
            print('Fermi energy: E_F = h * {:.2f} kHz'.format(self.E_F/(h_bar*2*np.pi)*1e-3))
            print('Fermi temperature: T_F = {:.2f} nK'.format(self.T_F*1e9))
            print('Reduced temperature: T/T_F = {:.2f}'.format(self.T_atoms/self.T_F))
            print('Harmonic oscillator length: a_HO = {:.2f} nm'.format(self.a_HO*1e9))
            print('Fermi radius: R_F = {:.2f} um'.format(self.R_F*1e6))
            print('Fugacity: z = {:.8f}'.format(self.fugacity))
            print('Chemical potential: μ = {:.2f} E_F'.format(self.mu/self.E_F))
            print('Energy per particle: E_tot/N = {:.2f} kHz'.format(self.E_tot/(h_bar*2*np.pi)*1e-3/self.N_atoms))
            print('Entropy per particle S_tot/N = k_B * {:.2f}'.format(self.S_tot/self.N_atoms))

    def density_distribution(self, x: float, y: float, z: float) -> float:
        """Calculates the real-space density at the specified (x,y,z)-coordinate.

        Parameters
        ----------
        x : float
            x-coordinate
        y : float
            y-coordinate
        z : float
            z_coordinate

        Returns
        -------
        float
            The real-space density.
        """
        if self.regime == '3D':
            V3D = lambda xx, yy, zz: 1/2 * self.M_atoms * (self.omega_x**2 * xx**2 + self.omega_y**2 * yy**2 +
                                                           self.omega_z**2 * zz**2)
            return -(self.M_atoms*k_B*self.T_atoms/2/np.pi/h_bar**2)**(3/2) * \
                rpolylog(3/2, -self.fugacity * np.exp(-V3D(x, y, z)/k_B/self.T_atoms))

        elif self.regime == '2D':
            V2D = lambda yy, zz: 1/2 * self.M_atoms * (self.omega_y**2 * yy**2 + self.omega_z**2 * zz**2)
            return self.M_atoms*k_B*self.T_atoms/2/np.pi/h_bar**2 * \
                np.log(1+self.fugacity*np.exp(-V2D(y, z)/k_B/self.T_atoms)) * \
                (self.M_atoms*self.omega_x/np.pi/h_bar)**(1/2) * np.exp(-self.M_atoms*self.omega_x/2/h_bar * x**2)
        elif self.regime == '1D':
            V1D = lambda zz: 1/2 * self.M_atoms * self.omega_z**2 * zz**2
            return -(self.M_atoms*k_B*self.T_atoms/2/np.pi/h_bar**2)**(1/2) * \
                rpolylog(1/2, -self.fugacity * np.exp(-V1D(z)/k_B/self.T_atoms)) * \
                self.M_atoms/np.pi/h_bar*(self.omega_x*self.omega_y)**(1/2) * \
                np.exp(-self.M_atoms/2/h_bar*(self.omega_x*x**2 + self.omega_y*y**2))
