import numpy as np
from scipy.special import gamma
from scipy.interpolate import interp1d
import warnings
import sys

### HELPER FUNCTIONS ###

try:
    from fdint import fdk

    def rpolylog(s, z):
        return -fdk(s-1, np.log(-z))/gamma(s)

except ImportError:
    warnings.warn('Unable to import fdint library! Using slow implementation of polylog from mpmath.')
    import mpmath as mp

    casted_poly = np.vectorize(lambda *a: float(mp.chop(mp.fp.polylog(*a))),
                               excluded=(0,), otypes=[float])

    def rpolylog(s, z):
        return casted_poly(s, z)
