import numpy as np
from numpy import exp, log, pi
import matplotlib.pyplot as plt
import copy
from scipy.optimize import least_squares
from scipy.signal import find_peaks
from .consts import c, k_B, h_bar, u
from .utilities import rpolylog
from .optical_lattice import AccordionLattice
from .fermi_gas import NonIntFermiGas


class LatticeLoadingModel(object):

    def __init__(self, fermi_gas: NonIntFermiGas, initial_pot, 
                 polarizability_func, regime='3D', offset: float = 0):
        """Create a new LatticeLoadingModel object which provides methods to calculate the distribution of atoms
        when loading a gas into a lattice along one direction.
        
        Parameters
        ----------
        fermi_gas : NonIntFermiGas
            The Fermi gas which will be loaded into the lattice.
        initial_pot : function(x, y, z)
            A function which returns the potential distribution of the trap holding the Fermi gas right before the 
            loading operation. Can be just the harmonic approximation of the potential.
        polarizability_func : function(wavelength)
            A function which takes the wavelength of the lattice potential and returns the polarizability of the atoms
            at this wavelength.
        regime : str, optional
            The spatial regime in which the gas will be AFTER the loading process, by default '3D'.
        offset : float, optional
            An offset between the center of the Fermi gas and the center of the new lattice potential, by default 0.
        
        Raises
        ------
        ValueError
            Raised when an incorrect identifier for a spatial regime is supplied.
        """

        self.fermi_gas = fermi_gas
        self.initial_pot = initial_pot
        self.polarizability_func = polarizability_func
        if regime in ['1D', '2D', '3D']:
            self.regime = regime
        else:
            raise ValueError('Unknown regime. Possibilities: [1D, 2D, 3D]')
        self.offset = offset

    def number_entropy_dist(self, mu_center, temp, vertical_freq, lat_omega_bar, site_centers):
        mu_loc = mu_center - 1/2*self.fermi_gas.M_atoms*vertical_freq**2 * (site_centers)**2
        fug_loc = np.exp(mu_loc/k_B/temp)
        if self.regime == '3D':
            T_red_loc = (-6 * rpolylog(3, -fug_loc))**(-1/3)
            N_loc = -k_B**3 * temp**3 * rpolylog(3, -fug_loc) / h_bar**3 / lat_omega_bar**3
            E_tot_loc = - 3 * k_B**4 * temp**4 * rpolylog(4, -fug_loc) / h_bar**3 / lat_omega_bar**3
            S_tot_loc = (E_tot_loc-mu_loc*N_loc) / k_B / temp - k_B**3 * temp**3 / h_bar**3 / lat_omega_bar**3 * \
                         rpolylog(4, -fug_loc)
        elif self.regime == '2D':
            T_red_loc = (-2 * rpolylog(2, -fug_loc))**(-1/2)
            N_loc = -k_B**2 * temp**2 * rpolylog(2, -fug_loc) / h_bar**2 / lat_omega_bar**2
            E_tot_loc = - 2 * k_B**3 * temp**3 * rpolylog(3, -fug_loc) / h_bar**2 / lat_omega_bar**2
            S_tot_loc = (E_tot_loc-mu_loc*N_loc) / k_B / temp - k_B**2 * temp**2 / h_bar**2 / lat_omega_bar**2 * \
                         rpolylog(3, -fug_loc)
        else:
            T_red_loc = 1/log(1+fug_loc)
            N_loc = k_B * temp * log(1+fug_loc) / h_bar / lat_omega_bar
            E_tot_loc = - k_B**2 * temp**2 * rpolylog(2, -fug_loc) / h_bar / lat_omega_bar
            S_tot_loc = (2 * E_tot_loc - mu_loc * N_loc) / k_B / temp
        return N_loc, S_tot_loc, T_red_loc

    def sum_number_entropy(self, x, vertical_freq, lat_omega_bar, site_centers):
        mu_center, temp = x * (h_bar*2*np.pi, 1e-6)
        N_loc, S_tot_loc, _ = self.number_entropy_dist(mu_center, temp, vertical_freq, lat_omega_bar, site_centers)
        return np.array([np.sum(N_loc), np.sum(S_tot_loc)])

    def calculate_site_dist(self, vertical_freq, lat_omega_bar, site_centers):

        x0 = (self.fermi_gas.mu/(h_bar*2*np.pi), self.fermi_gas.T_atoms*1e6)

        res = least_squares(lambda x: self.sum_number_entropy(x, vertical_freq, lat_omega_bar, site_centers) -
                                      (self.fermi_gas.N_atoms, self.fermi_gas.S_tot), x0,
                            bounds=((-np.inf, 0), (np.inf, 100)))

        mu_opt, temp_opt = res.x * (h_bar*2*np.pi, 1e-6)

        opt_params = self.number_entropy_dist(mu_opt, temp_opt, vertical_freq, lat_omega_bar, 
                                                        site_centers)

        return opt_params, mu_opt, temp_opt

    def load_lattice(self, site_centers, lat_omega_bar, lat_env_omega = 0):
        """Calculate the distribution of cloud parameters after loading the gas into a lattice potential. The lattice
        potential is parametrized by the location of the site centers as well as the (mean) trapping frequency in a 
        lattice site. 

        For better results, the trap frequency of the lattice envelope along the loading direction should be supplied.
        
        Parameters
        ----------
        site_centers : Array[float]
            A numpy array containing the positions of the site centers. The positions should be symmetric about 0, the 
            offset specified when initializing the class is subtracted internally. Note that for best results, the 
            range of site centers should span the whole cloud.
        lat_omega_bar : float
            Mean trapping frequency of the potential in a lattice site.
        lat_env_omega : float, optional
            Frequency of the lattice envelope along the loading direction, by default 0.
        
        Returns
        -------
        Tuple[Array[float], Array[float], Array[float], float, float]
            Returns a tuple containing the distribution of atom numbers, the entropy distribution, the distribution of 
            reduced temperatures, the central chemical potential and the temperature.
        """
        
        site_centers -= self.offset

        if self.regime in ['2D', '3D']: #load from 3D->3D/2D, new lattice is along x direction
            om_outer = (self.fermi_gas.omega_x ** 2 + lat_env_omega ** 2) ** (1 / 2)
        else: # load from 2D->1D, new lattice is along y direction
            om_outer = (self.fermi_gas.omega_y ** 2 + lat_env_omega ** 2) ** (1 / 2)

        (N, S, Tr), mu0, T = self.calculate_site_dist(om_outer, lat_omega_bar, site_centers)

        return N, S, Tr, mu0, T

    def _acc_loading_step(self):
        pass

    def load_accordion_lattice(self, accordion_lattice: AccordionLattice, bounds=(-150*1e-6, 150*1e-6), 
                               loading_point='thermal hop', temp_multiplier=3, verbose=False, plot=False, end_power_fact=0.3, num_power_steps = 50):
        """Calculate the distribution of cloud parameters after loading the gas into a lattice potential. The lattice is
        parametrized as an AccordionLattice object. 

        It is important to supply the correct bounds, which determine the region along the loading direction over which 
        the site distribution is calculated.
        
        Parameters
        ----------
        accordion_lattice : AccordionLattice
            An AccordionLattice describing the lattice into which the gas is loaded.
        bounds : Tuple[float], optional
            The region over which the site distribution is calculated, by default (-150*1e-6, 150*1e-6).
        loading_point : str, optional
            The condition after which the loading point is determined. poss, by default 'thermal hop'
        temp_multiplier : int, optional
            Determined the temperature multiplier x for thermal hopping loading point determination, i.e. the condition 
            U0 = E_F + x * k_B*T, by default 3.
        verbose : bool, optional
            Print verbose information about the loading point determination, by default False.
        plot : bool, optional
            Plot the lattice potential for individual lattice powers while determining the loading point, by default 
            False.
        
        Returns
        -------
        Tuple[Array[float], Array[float], Array[float], float, float]
            Returns a tuple containing the distribution of atom numbers, the entropy distribution, the distribution of 
            reduced temperatures, the central chemical potential and the temperature.
        
        Raises
        ------
        ValueError
            Raised when an incorrect identifier for the method, according to which the loading point is determined, is 
            specified.
        """
        
        lb, ub = bounds
        x = np.linspace(lb, ub, 1000)

        final_params = 0
        site_centers = []
        temp = 0
        center_ind = 0

        if plot:
            fig, ax = plt.subplots()

        if loading_point not in ['final power', 'thermal hop', 'tunneling']:
            raise ValueError('Unknown specification of loading point. '
                             'Possibilities: [final power, thermal hop, tunneling]')
        
        if loading_point == 'final power':
            raise NotImplementedError('Not yet implemented.')
        elif loading_point == 'thermal hop':
            power_factors = np.linspace(0.001, end_power_fact, num_power_steps)

            for i, pf in enumerate(power_factors):
                b1, b2 = copy.deepcopy(accordion_lattice.beam_1), copy.deepcopy(accordion_lattice.beam_2)
                b1.power *= pf
                b2.power *= pf
                b1.recalculate()
                b2.recalculate()
                alat = AccordionLattice(b1, b2)

                pot_cmb = lambda x: self.initial_pot(x) + alat.potential_dist((x - self.offset), 0, 1e-99,
                                                                              self.polarizability_func(b1.wavelength))
                
                # Find the peak positions (the positions are unchanged by the presence of the ODT potential)
                maxima_pos, _ = find_peaks(alat.potential_dist((x - self.offset), 0, 1e-99,
                                                               self.polarizability_func(b1.wavelength)))
                
                # Find the positions of the minima, i.e. the site centers
                minima_pos, _ = find_peaks(-pot_cmb(x))
                minima_vals = pot_cmb(x[minima_pos])
                site_centers = x[minima_pos]
                
                # Find the position of the global minimum (should be the central site for small displacements)
                global_min_ind = np.where(minima_vals == minima_vals.min())[0][0]
                global_min_pos = minima_pos[global_min_ind]
                global_min_val = pot_cmb(x[global_min_pos])

                # Find the positions of the maxima surrounding the global minimum
                first_max_right = maxima_pos[maxima_pos > global_min_pos][0]
                first_max_left = maxima_pos[maxima_pos < global_min_pos][-1]
                
                if plot:
                    ax.plot(x*1e6, pot_cmb(x)/k_B*1e6)
                    ax.scatter(x[maxima_pos]*1e6, pot_cmb(x[maxima_pos])/k_B*1e6)
                    ax.scatter(x[minima_pos]*1e6, pot_cmb(x[minima_pos])/k_B*1e6)

                # Calculate the depth of the central trap
                central_trap_depth = min(pot_cmb(x[first_max_right]), pot_cmb(x[first_max_left])) - global_min_val

                # Get frequencies of central site in accordion lattice and vertical frequency of envelope
                (omxa, omya, omza), _, _ = alat.calc_trap_params(self.polarizability_func(b1.wavelength),
                                                                 m_atoms=self.fermi_gas.M_atoms)
                lat_env_omx, _, _ = alat.get_envelope_frequencies(self.polarizability_func(b1.wavelength),
                                                                  m_atoms=self.fermi_gas.M_atoms)

                # Calculate frequencies of combined ODT and accordion potential (only accurate for central sites)
                omx = (omxa ** 2 + self.fermi_gas.omega_x ** 2) ** (1 / 2)
                omy = (omya ** 2 + self.fermi_gas.omega_y ** 2) ** (1 / 2)
                omz = (omza ** 2 + self.fermi_gas.omega_z ** 2) ** (1 / 2)
                if self.regime=='3D':
                    ombar = (omx * omy * omz) ** (1 / 3)
                else:
                    ombar = (omy * omz) ** (1/2)

                omx_outer = (self.fermi_gas.omega_x ** 2 + lat_env_omx ** 2) ** (1 / 2)

                (N, S, Tr), mu0, T = self.calculate_site_dist(omx_outer, ombar, site_centers)
                Nc = N[global_min_ind]
                if self.regime=='3D':
                    EFc = h_bar * ombar * (6*Nc)**(1/3)
                else:
                    EFc = h_bar * ombar * (2*Nc)**(1/2)
                if verbose:
                    print('Power factor: {:.2f}, EFc: {:.2f} kHz, T: {:.2f} kHz, ωx: {:.2f} kHz, ' \
                          'U: {:.2f} kHz'.format(pf, EFc/h_bar/2/np.pi*1e-3, k_B*T/h_bar/2/np.pi*1e-3, 
                                                 omx/2/np.pi*1e-3, central_trap_depth/h_bar/2/np.pi*1e-3))

                # When loading point is reached, break loop and set final variables
                if central_trap_depth >= (EFc + temp_multiplier * k_B * T):
                    if verbose:
                        print('Loading point reached for P = {:.2f}*P_final'.format(pf))
                    final_params = N, S, Tr
                    temp = T
                    center_ind = global_min_ind
                    break

        elif loading_point == 'tunneling':
            raise NotImplementedError('Determination of loading point according to suppressed tunneling is not '
                                      'yet implemented.')
        return final_params, site_centers, center_ind, temp


    def plot_final_dist(self):
        pass
