# Optical Lattices Python Library

Simulate parameters and interference patterns of optical lattices and
simulate loading of a Fermi gas from an optical dipole trap.

## Installation

You can install `optical_traps` directly via pip

```shell
$ pip install git+https://gitlab.physik.uni-muenchen.de/A.Impertro/optical-traps.git
```

If desired, you can install the optional dependency
[`fdint`](https://pypi.org/project/fdint/), which
allows calculations involving Fermi-Dirac integrals to be performed
faster.

```shell
$ pip install git+https://gitlab.physik.uni-muenchen.de/A.Impertro/optical-traps.git[fdint]
```

In development, you can install the module from your local
repository by running

```shell
$ pip install -e .
```
